﻿using UnityEngine;
using System.Collections;

public class meshDeformer : MonoBehaviour {

	public Transform 	reference;
	public float 		displacementAmount;
	public float 		timeDumpener;

	Mesh myMesh;
	Vector3[] originalPoints;

	void Start () 
	{
		myMesh = GetComponent<MeshFilter>().mesh;
		originalPoints = new Vector3[ myMesh.vertexCount ];
		myMesh.MarkDynamic();

		for( int i = 0; i< myMesh.vertexCount; i++ )
		{
			originalPoints[ i ] = new Vector3 ( myMesh.vertices[i].x, myMesh.vertices[i].y, myMesh.vertices[i].z );

		}

	}
	
	void Update () 
	
	{
		Vector3 refPoints = reference.position;

		Vector3[] newVert = myMesh.vertices;
		for( int i = 0; i< myMesh.vertexCount; i++)
		{
			Vector3 pt = originalPoints [ i ];
			Vector3 direction = ( pt - refPoints );
			float timeMul = ( pt - refPoints).magnitude * timeDumpener;

			direction.Normalize();


			newVert [i] = originalPoints [i] + direction * Mathf.Sin(Time.time * timeMul) * displacementAmount;

		}

		myMesh.vertices =  newVert;
		myMesh.RecalculateNormals();


	}
}
